<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;

    protected $table = 'articles';

    public function carousel()
    {
        return $this->belongsTo('App/Carousel');
    }
    
    public function articleMedia()
    {
        return $this->hasOne('App\MediaArticle');
    }
}
