<?php

namespace App\Http\Controllers\Admin\Accueil;

use App\Content;
use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccueilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd( Menu::where( 'title' , 'accueil' )->with('contents')->get());

        return view('admin.Accueil.accueil', [
            'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
            'categories' => DB::table('menus')
                                    ->join('contents', 'contents.menu_id', '=', 'menus.id')
                                    ->where('menus.title', 'accueil')
                                    ->select('menus.title As M_title' , 'contents.*' )
                                    ->get()
        ] );
    }
}
