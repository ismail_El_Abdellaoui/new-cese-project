<?php

namespace App\Http\Controllers\Admin\Accueil;

use App\Article;
use App\Carousel;
use App\Http\Controllers\Controller;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Null_;

class CarouselController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.Accueil.carousel.silds', [
            'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
            'slids' =>  DB::table('articles')
                                    ->join('carousels', 'carousels.article_id', '=', 'articles.id')
                                    ->where('carousels.selected', 1)
                                    ->select('articles.*', 'carousels.id as carousel_id' ,
                                            'carousels.updated_at as carousel_update')
                                    ->get()
        ] );
    }


    public function store(Request $request)
    {
    // if modification du slide
        if ($request->old_id) {
            $old = Carousel::findOrFail($request->old_id);
            $old->selected = 0 ;
            $old->save();

            $request->session()->flash('status' , "Diapositive modifiée");    
        }
    // nouveau slide ou modification
        $carousel = new Carousel();
        if($request->id_choisie == null ){
            return Redirect::back();
        }
        $carousel->article_id = $request->id_choisie;
        $carousel->selected = 1;
        $media_type = null;
        if ($request->hasFile('back_img')) {
            $media_type = $request->file('back_img')->extension();
        }
            if ($media_type == 'svg' || $media_type == 'png' || $media_type == 'jpeg' || 
                $media_type == 'gif' || $media_type == 'jpg' ) {

                    $carousel->back_img = $request->file('back_img')->store('carousel' , 'public');
            }elseif( $media_type == null ){
                $carousel->back_img = "carousel/default.jpeg";
            }else{
                return 'file selected is not image';
            }
        $carousel->save();

        if (!$request->old_id) {
            $request->session()->flash('status' , "Nouvelle diapositive crée");    
        }
        return redirect()->route('A_carousel.index');
    }

    public function edit($id)
    {
        return view('admin.accueil.carousel.edit' ,[
            'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
            'articles' =>  Article::whereNotIn('articles.id', 
                                                Carousel::where('selected', 1)
                                                ->select('article_id')
                                                ->get())
                                    ->select('*')
                                    ->get(),    
            'carousel_id' => $id
        ]);
    }

    public function newSlide()
    {
        return view('admin.accueil.carousel.newSlide' ,[
            'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
            'articles' =>  Article::whereNotIn('articles.id', 
                                                Carousel::where('selected', 1)
                                                ->select('article_id')
                                                ->get())
                                    ->select('*')
                                    ->get()
        ]);
    }
}