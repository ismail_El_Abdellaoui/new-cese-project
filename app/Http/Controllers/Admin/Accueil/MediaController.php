<?php

namespace App\Http\Controllers\Admin\Accueil;

use App\Article;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMedia;
use App\Media;
use App\MediaArticle;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.Accueil.media.media_categorie', [
            'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
            'media_categories' => Media::all() 
            ]);
    }

    public function create()
    {
        // DB::connection()->enableQueryLog();
        $media_articles = MediaArticle::select('article_id')->get();
        $articles = Article::whereNotIn('id' , $media_articles)
                            ->get() ;
        return view('admin.accueil.media.media_add_article' , [
            'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
            'media_categories' => Media::all() ,
            'articles' => $articles
        ]);
    }

    public function store(StoreMedia $request)
    {
        $media_art = MediaArticle::all();
        // dd($media_art);
        foreach ($media_art as $key ) {
            if ($key != null ) {
                // dd($key);
                if ($key->media_id == $request->media_id && $key->article_id == $request->article_selected) {
                    return back()->with('status' , 'Déjà existe');
                }
            }
        }
        foreach ( $request->article_selected as $article ) {
            $media_article = new MediaArticle();
            $media_article->token =  $request->_token ;
            $media_article->media_id =  $request->media_id ;
            $media_article->article_id =  $article ;
            $media_article->save();
        }

        $request->session()->flash('status' , "Articles ajouter");

        return redirect()->route('A_media.index');
    }

    public function show($id)
    {
        $articles = DB::table('articles')
                        ->join('media_articles', 'media_articles.article_id', '=', 'articles.id')
                        ->join('media', 'media.id', '=', 'media_articles.media_id')
                        ->where([
                            ['media_articles.media_id' , $id],
                             ['media_articles.deleted_at' , NULL]
                        ])
                        ->select('articles.*' , 'media_articles.*' ,'media.title as media_title' )
                        ->get();

        return view('admin.Accueil.media.media_articles', [
                'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
                'articles' =>$articles  
        ]);
    }

    public function delete(Request $request)
    {
        MediaArticle::where([
            ['article_id', $request->article_id],
            ['media_id', $request->media_id]
        ])->delete();

        $request->session()->flash('status' , "Article supprimer");

        return redirect()->route('A_media.show', [ $request->media_id])->with('msg', 'Deleted ');
    }
}
