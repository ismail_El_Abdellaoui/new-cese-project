<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Article;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd( Menu::with('sousMenu')->get());
        return view('admin.A_master', [
            'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
             'articles' => Article::get()
        ] );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.Accueil.accueil', [
            'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
            'categories' => DB::table('menus')
                                    ->join('contents', 'contents.menu_id','=' , 'menus.id'  )
                                    ->where('menus.id', $id )
                                    ->select('contents.*', 'menus.title As Menu_title', 'menus.id As M_id' )
                                    ->get()
        ] );
    }
}
