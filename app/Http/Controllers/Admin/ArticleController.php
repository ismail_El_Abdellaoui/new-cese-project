<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Article;
use App\Carousel;
use App\Http\Requests\StoreArticle;
use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\HttpCache\Store;
use Illuminate\Support\Str;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.article.articles', [
            'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
             'articles' => Article::get()
        ] );
    }

    public function deletedArticle(){
        $deleted_articles = Article::onlyTrashed()
                            ->get();
        return view('admin.article.deletedArticles', [
            'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
            'articles' => $deleted_articles
        ] );
    }

    public function showDeleted($id)
    {
        $article = Article::onlyTrashed()
                    ->where('id', $id)
                    ->first();
        return view('admin.article.article', [
            'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
            'article' => $article
        ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.article.addArticle', [
            'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get()
        ] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArticle $request)
    {
        $request->validate([
            'media' => 'required'
        ]);

        $article = new Article();
        $article->title = $request->input('title');
        $article->content = $request->input('content');
        $article->created_by = 'user aute';
        if ($request->hasFile('media')) {
            $article->media = $request->file('media')->store('article' , 'public');
            $media_type = Str::lower($request->file('media')->extension());
            // dd($media_type);
            if ($media_type == 'svg' || $media_type == 'png' || $media_type == 'jpeg' || 
                $media_type == 'gif' || $media_type == 'jpg' || $media_type == 'psd' || 
                $media_type == 'pdf' || $media_type == 'ia' ) {
                    $article->media_type = 'img';
            }elseif($media_type == 'mp4' ){
                $article->media_type = 'video';
            }
        }
        $article->save();

        $request->session()->flash('status' , "article créé");

        return redirect()->route('A_article.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.article.article', [
            'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
            'article' => Article::findOrFail($id)
        ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.article.edit' ,[
                'menus' => Menu::where('parent_id' , NULL)->with('sousMenu')->get(),
                'article' => Article::findOrFail($id)
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreArticle $request, $id)
    {
        // dd($request);
        $article = Article::findOrFail($id);

        $article->title = $request->title;
        $article->content = $request->content;
        if ($request->hasFile('media')) {
            if (Storage::delete($article->media)) {
                $article->media = $request->file('media')->store('article' , 'public');
            }
        }
        $article->save();

        $request->session()->flash('status' , "Article ($id) modifiée");

        return redirect()->route('A_article.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { 
        // dd($id);
        if (DB::table('articles')
                                ->join('carousels', 'carousels.article_id', '=', 'articles.id')
                                ->where('carousels.article_id', $id)
                                ->where('carousels.selected', 1)
                                ->select('carousels.*')
                                ->get()->count() > 0) {
            return 'article is selected can not be deleted';
        }
        else{
            Article::destroy($id);

            Session::flash('status' , "Article ($id) supprimer");
            
            return redirect()->route('A_article.index');
        }
    }
}
