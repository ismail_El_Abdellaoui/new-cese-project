<?php

namespace App\Http\Controllers\Pub;

use App\Article;
use App\Http\Controllers\Controller;
use App\Media;
use App\MediaArticle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AccueilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('public.accueil' , [
            'carousel' =>  DB::table('articles')
                                ->join('carousels', 'carousels.article_id', '=', 'articles.id')
                                ->where('carousels.selected', 1)
                                ->select('articles.*', 'carousels.id as carousel_id' , 'carousels.back_img')
                                ->get(),
            'media' => Media::all(),
            'accueil' =>  DB::table('media_articles')
                                ->join('articles', 'media_articles.article_id', '=', 'articles.id')
                                ->where('media_articles.media_id', 1)
                                ->limit(10)
                                ->select('articles.*')
                                ->get(),
            'press' =>  DB::table('media_articles')
                                ->join('articles', 'media_articles.article_id', '=', 'articles.id')
                                ->where('media_articles.media_id', 2)
                                ->limit(10)
                                ->select('articles.*')
                                ->get(),
            'conseil' =>  DB::table('media_articles')
                                ->join('articles', 'media_articles.article_id', '=', 'articles.id')
                                ->where('media_articles.media_id', 3)
                                ->limit(10)
                                ->select('articles.*')
                                ->get(),

        ]);
    }
}
