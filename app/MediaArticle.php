<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MediaArticle extends Model
{
    use SoftDeletes;

    protected $table = 'media_articles';
    
    public function media()
    {
        return $this->belongsTo('App\Media');
    }
    public function mediaArticle()
    {
        return $this->belongsTo('App\Article');
    }
}
