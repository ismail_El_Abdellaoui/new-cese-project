<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public function menu()
    {
        return $this->belongsTo(self::class, 'id');
    }

    public function sousMenu()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function contents()
    {
        return $this->hasMany('App\Content');
    }
}
