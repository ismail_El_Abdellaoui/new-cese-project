<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_articles', function (Blueprint $table) {
            $table->string('token' , 255);
            $table->foreignId('article_id')->constrained();
            $table->foreignId('media_id')->constrained();
            $table->primary([ 'token' ,'media_id', 'article_id' ]);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_articles');
    }
}
