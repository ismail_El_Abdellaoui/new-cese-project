/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/public/P_master.js":
/*!*****************************************!*\
  !*** ./resources/js/public/P_master.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  if (!sessionStorage.nav) {
    sessionStorage.setItem("nav", "accueil");
    console.log(sessionStorage.nav);
  } else {
    console.log(sessionStorage.nav);
  }

  $("#" + sessionStorage.nav).addClass('active');
  $('.js-nav').click(function (event) {
    console.log($(this).attr('id'));
    sessionStorage.setItem("nav", $(this).attr('id'));
  });
});

/***/ }),

/***/ "./resources/js/public/accueil.js":
/*!****************************************!*\
  !*** ./resources/js/public/accueil.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  // slider 
  $('.swiper-slide').slick({
    dots: false,
    infinite: false,
    slidesToShow: 6,
    slidesToScroll: 1
  });
  $(document).ajaxSuccess(function (event, request, settings) {
    $('.swiper-slide').slick({
      dots: false,
      infinite: false,
      slidesToShow: 6,
      slidesToScroll: 1
    });
  }); // media articles

  $(".toggle-article").click(function (event) {
    var id = $(this).attr('id');
    $(".toggle-article").removeClass("down");

    if (id == 1) {
      $(this).addClass("down");
      $('#press-DD').removeClass("opened");
      $('#conseil-DD').removeClass("opened");
      $('#accueil-DD').addClass("opened");
    }

    if (id == 2) {
      $(this).addClass("down");
      $('#conseil-DD').removeClass("opened");
      $('#accueil-DD').removeClass("opened");
      $('#press-DD').addClass("opened");
    }

    if (id == 3) {
      $(this).addClass("down");
      $('#accueil-DD').removeClass("opened");
      $('#press-DD').removeClass("opened");
      $('#conseil-DD').addClass("opened");
    }

    event.stopPropagation();
    return false;
  });
});

/***/ }),

/***/ "./resources/js/public/article.js":
/*!****************************************!*\
  !*** ./resources/js/public/article.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  var text = $('.article-title');
  var max = 30;
  text.click(function (event) {// alert(text.text().length);
  });
});

/***/ }),

/***/ 3:
/*!*****************************************************************************************************************!*\
  !*** multi ./resources/js/public/accueil.js ./resources/js/public/article.js ./resources/js/public/P_master.js ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\xampp\htdocs\new-CESE-Project\new-cese-project\resources\js\public\accueil.js */"./resources/js/public/accueil.js");
__webpack_require__(/*! C:\xampp\htdocs\new-CESE-Project\new-cese-project\resources\js\public\article.js */"./resources/js/public/article.js");
module.exports = __webpack_require__(/*! C:\xampp\htdocs\new-CESE-Project\new-cese-project\resources\js\public\P_master.js */"./resources/js/public/P_master.js");


/***/ })

/******/ });