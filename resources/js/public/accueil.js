$(document).ready(function(){

      // slider 

      $('.swiper-slide').slick({
        dots: false,
        infinite: false,
        slidesToShow: 6,
        slidesToScroll: 1
      }); 

      $(document).ajaxSuccess(function (event, request, settings) {

          $('.swiper-slide').slick({
            dots: false,
            infinite: false,
            slidesToShow: 6,
            slidesToScroll: 1
          }); 
      });
      // media articles
        $(".toggle-article").click(function(event){
          var id = $(this).attr('id');
          $(".toggle-article").removeClass("down");          
  
          if (id == 1) {
            $(this).addClass("down");          
            $('#press-DD').removeClass("opened");
            $('#conseil-DD').removeClass("opened");
            $('#accueil-DD').addClass("opened");          
          }
          if (id == 2) {
            $(this).addClass("down");  
            $('#conseil-DD').removeClass("opened");
            $('#accueil-DD').removeClass("opened"); 
            $('#press-DD').addClass("opened");
                    
          }
          if (id == 3) {
            $(this).addClass("down");  
            $('#accueil-DD').removeClass("opened");          
            $('#press-DD').removeClass("opened");
            $('#conseil-DD').addClass("opened");
          }
          
  
          event.stopPropagation();
          return false
        });
    });