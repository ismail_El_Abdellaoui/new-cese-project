@extends('admin.A_master')

@section('admin_content')

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
      <div class="row mb-2">
          <div class="col-sm-6 pl-2">
              <h1 class="m-0 text-dark">Accueil categories</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 pr-2">
              {{-- <a href="{{ route('A_article.create') }}" class="btn btn-success header-btn">Nouveau un article</a> --}}
          </div><!-- /.col -->
      </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<table class="table table-striped col-auto">
    <thead>
      <tr>
        <th scope="col">titre</th>
        <th scope="col">menu title</th>
        <th scope="col">action</th>
      </tr>
    </thead>
    <tbody>
        {{-- {{dd($content)}} --}}
    @foreach ($categories as $content)
        <tr>
            <td>{{$content->title}}</td>
            <td>{{$content->Menu_title}}</td>
            <td class="content-fluid">
                <a href=" {{ route("A_$content->title.index") }} " class="btn btn-success" >consulté</a>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>

@endsection
