@extends('admin.A_master')

@section('admin_content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6 pl-2">
        <h1 class="m-0 text-dark">liste des articles</h1>
      </div><!-- /.col -->
      <div class="col-sm-6 pr-2">
        <a href="{{ route('A_article.create') }}" class="btn btn-success header-btn">ajouter un article</a>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<form action="{{route('A_carousel.store')}}" method="POST" class="send-forme" enctype="multipart/form-data">
  @csrf
    
  <input type="hidden" name="old_id" value="{{ $carousel_id }}">
  <input type="hidden" class="id_choisie mx-2 px-1" name="id_choisie" id="id_choisie" value="">
  <div class="my-3">
    <label class="mx-2 px-1">article choisi</label>
  <input type="number" class=" id_choisie mx-2 px-1" disabled="disabled">
  <input type="text" class="article_name mx-2 px-1" disabled="disabled">
  </div>
  
  <div class="my-3">
  <label class="mx-2 px-1" for="">Back groud image</label>
  <input type="file" name="back_img" >
  </div>
  
  <input type="submit" class="btn btn-primary mx-2 px-1 align-content-center" value="Modifier">
  <a href="{{url()->previous()}}" class="btn btn-danger">annuler</a>
</form>

<table class="table table-striped col-auto">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">titre</th>
        <th scope="col">action</th>
        <th scope="col">selectionne</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($articles as $article)
        <tr>
            <th scope="row">{{$article->id}}</th>
            <td>{{$article->title}}</td>
            <td class="content-fluid">
                <a href="{{ route('A_article.show', [ $article->id ]) }}" class="btn btn-success" >consulté</a>
            </td>
            <td>
                  <input type="radio" name="new-slid" class="slid-select" id="{{$article->title}}" value="{{$article->id}}">
            </td>
        </tr>
    @endforeach
    </tbody>
  </table>


@endsection
