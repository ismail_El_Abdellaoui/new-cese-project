@extends('admin.A_master')

@section('admin_content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6 pl-2">
                    <h1 class="m-0 text-dark">Carousel slides</h1>
                </div><!-- /.col -->
                <div class="col-sm-6 pr-2">
                    <a href="{{ route('A_article.create') }}" class="btn btn-success header-btn">nouvel article</a>
                    @if ( $slids->count()  < 3 )
                        <a href="{{ route('newSlide') }}" class="btn btn-primary header-btn">nouveau slide</a>
                    @endif
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    {{-- flash message --}}
    @if (session()->has('status'))
        <h3 class="flash-msg">
            {{ session()->get('status') }}
        </h3>
    @endif
    {{-- end flash message --}}

    <table class="table table-striped col-auto">
        <thead>
          <tr class="row">
            <th scope="col" >#</th>
            <th scope="col" class="col-6">article titre</th>
            <th scope="col" class="col">date Sellection</th>
            <th scope="col" class="col">action</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($slids as $slid)
            <tr class="row">
                <th scope="row">{{$slid->id}}</th>
                <td class="col-6">{{$slid->title}}</td>
                <td class="col">{{$slid->carousel_update}}</td>
                <td class="content-fluid col">
                    <a href="{{ route('A_article.show', [ $slid->id ]) }}" class="btn btn-success" >consulté</a>
                    <a href="{{ route('A_carousel.edit', [ $slid->carousel_id ]) }}" class="btn btn-primary" >modifier</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    
    
@endsection
