@extends('admin.A_master')

@section('admin_content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6 pl-2">
                    <h1 class="m-0 text-dark">ajouter de nouveaux article </h1>
                </div><!-- /.col -->
                <div class="col-sm-6 pr-2">
                    <a href="{{ route('A_article.create') }}" class="btn btn-success header-btn">nouvel un article</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <form action="{{ route('A_media.store') }}" class="send-forme" method="post">
        @csrf

        <div class="row">
            <label for="media">Selectionne une categorie</label>
            <select name="media_id" id="media">
                <option value=""></option>
                @foreach ($media_categories as $categorie)
                    <option value="{{ $categorie->id }}">{{ $categorie->title }}</option>
                @endforeach
            </select>
            @if ($errors->first('media_id'))
                <span class="error-name">* {{ $errors->first('media_id') }}</span>
            @endif
        </div>
        
        <table class="table table-striped col-auto">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Article titre</th>
                <th scope="col">action</th>
                <th scope="col">select</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($articles as $article)
                <tr>
                    <th scope="row">{{$article->id}}</th>
                    <td>{{$article->title}}</td>
                    <td class="content-fluid">
                        <a href="{{ route('A_article.show', [ $article->id ]) }}" class="btn btn-success" >consulté</a>
                    </td>
                    <td><input type="checkbox" name="article_selected[]" id="{{ $article->id }}" value="{{ $article->id }}"></td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <input type="submit" class="btn btn-default add-articles" value="Add articles">
    </form>

    


    
    
    
@endsection
