@extends('admin.A_master')

@section('admin_content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6 pl-2">
                    @php($first_article = null)
                    @foreach ($articles as $article)
                        @php($first_article = $article)
                        @break
                    @endforeach
                    
                    @if ( $first_article )
                        <h1 class="m-0 text-dark">{{$first_article->media_title}} ARTICLES</h1>
                    @else
                        <h1 class="m-0 text-dark">No articles selected</h1>
                    @endif
                </div><!-- /.col -->
                <div class="col-sm-6 pr-2">
                    <a href="{{ route('A_article.create') }}" class="btn btn-success header-btn">nouvel un article</a>
                    <a href="{{ route('A_media.create') }}" class="btn btn-primary header-btn">ajouter un article</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    {{-- flash message --}}
    @if (session()->has('status'))
        <h3 class="flash-msg">
            {{ session()->get('status') }}
        </h3>
    @endif
    {{-- end flash message --}}


    @isset($msg)
        <p>{{$msg}}</p>
    @endisset

    <table class="table table-striped col-auto">
        <thead>
          <tr class="row">
            <th scope="col">#</th>
            <th scope="col" class="col-9">article titre</th>
            <th scope="col" class="col">action</th>
          </tr>
        </thead>
        <tbody>
        @foreach ($articles as $article)
            <tr class="row">
                <th scope="row">{{$article->id}}</th>
                <td class="col-9">{{$article->title}}</td>
                <td class="content-fluid col">
                    <a href="{{ route('A_article.show', [ $article->id ]) }}" class="btn btn-success" >consulté</a>
                    <form method="POST" action="{{ route('A_media_delete') }}"
                        style="display: inline;">
                      @csrf
                      
                      <input type="hidden" name="article_id" id="" value="{{$article->article_id}}">
                      <input type="hidden" name="media_id" id="" value="{{$article->media_id}}">
                      <input type="submit" class="btn btn-danger" value="supprimer">
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    
    
@endsection
