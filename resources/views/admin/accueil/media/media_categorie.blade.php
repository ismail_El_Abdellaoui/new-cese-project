@extends('admin.A_master')

@section('admin_content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6 pl-2">
                    <h1 class="m-0 text-dark">Media categories</h1>
                </div><!-- /.col -->
                <div class="col-sm-6 pr-2">
                    <a href="{{ route('A_article.create') }}" class="btn btn-success header-btn">nouvel un article</a>
                    <a href="{{ route('A_media.create') }}" class="btn btn-primary header-btn">ajouter un article</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    {{-- flash message --}}
    @if (session()->has('status'))
        <h3 class="flash-msg">
            {{ session()->get('status') }}
        </h3>
    @endif
    {{-- end flash message --}}

    <table class="table table-striped col-auto">
        <thead>
          <tr>
            <th scope="col">titre</th>
            <th scope="col">action</th>
          </tr>
        </thead>
        <tbody>
            {{-- {{dd($content)}} --}}
        @foreach ($media_categories as $categorie )
            <tr>
                <td>{{$categorie->title}}</td>
                <td class="content-fluid">
                    <a href=" {{ route("A_media.show" , [ $categorie->id ]) }} " class="btn btn-success" >consulté</a>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>
    
    
    
@endsection
