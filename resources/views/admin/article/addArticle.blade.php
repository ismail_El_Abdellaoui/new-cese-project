@extends('admin.A_master')

@section('admin_content')
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-12 pl-2">
          <h1 class="m-0 text-dark">ajouter un article</h1>
        </div><!-- /.col -->
      
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

    <form method="POST" action="{{ route('A_article.store') }}" enctype="multipart/form-data">
      @csrf
        
      @include('admin.article.forme')
      <button type="submit" class="btn btn-primary">ajouter</button>
      <a href="{{url()->previous()}}" class="btn btn-danger">annuler</a>
    </form>

@endsection
