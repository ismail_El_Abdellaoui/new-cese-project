@extends('admin.A_master')

@section('admin_content')
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6 pl-2">
          <h1 class="m-0 text-dark">Détail de l'article</h1>
        </div><!-- /.col -->
        <div class="col-sm-6 pr-2">
          <a href="{{ url()->previous() }}" class="btn btn-danger header-btn">retoure</a>
          @if ($article->deleted_at==NULL)
            <a href="{{ route('A_article.edit', [ $article->id ]) }}" class="btn btn-primary header-btn">modifier</a>
            <a href="{{ route('A_article.create') }}" class="btn btn-success header-btn">nouvel article</a>
            <a href="{{ route('A_article.index') }}" class="btn btn-secondary header-btn">liste article</a>
          @endif
          
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  {{-- content --}}
  <div class="content">
    <h3 class="article-title">{{$article->title}}</h3>
    <div class="body-content">
      <p class="text-content mb-5">
        {!!$article->Content!!}
      </p>
      <img src="{{asset('/storage//'. $article->media )}}" class="media" alt="">
    </div>
    
  </div>
  {{-- /.content --}}
@endsection
