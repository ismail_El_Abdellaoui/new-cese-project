@extends('admin.A_master')

@section('admin_content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6 pl-2">
        <h1 class="m-0 text-dark">liste des articles</h1>
      </div><!-- /.col -->
      <div class="col-sm-6 pr-2">
        <a href="{{ route('A_article.create') }}" class="btn btn-success header-btn">ajouter un article</a>
        <a href="{{ route('A_articlesDeleted') }}" class="btn btn-secondary header-btn">articles supprimés</a>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

@if (session()->has('status'))
  <h3 class="flash-msg">
    {{ session()->get('status') }}
  </h3>
@endif

<table class="table table-striped col-auto">
    <thead>
      <tr class="row">
        <th scope="col">#</th>
        <th scope="col" class="col-5">titre</th>
        <th scope="col" class="col">Créé à</th>
        <th scope="col" class="col-4">action</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($articles as $article)
        <tr class="row">
            <th scope="row">{{$article->id}}</th>
            <td class="col-5">{{$article->title}}</td>
            <td class="col">{{$article->created_at}}</td>
            <td class="content-fluid col-4">
                <a href="{{ route('A_article.show', [ $article->id ]) }}" class="btn btn-success" >consulté</a>
                <a href="{{ route('A_article.edit', [ $article->id ]) }}" class="btn btn-primary" >modifier</a>
                
                <form method="POST" action="{{ route('A_article.destroy', [ $article->id ]) }}" id="delete-{{$article->id}}"
                    style="display: inline;">
                  @csrf
                  @method('delete')
                  <button type="button" class="btn btn-danger delete-article" id="{{$article->id}}" 
                          data-toggle="modal" data-target="#modal-danger">
                    supprimer
                  </button>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
  </table>


  <div class="modal fade" id="modal-danger">
    <div class="modal-dialog">
      <div class="modal-content bg-danger">
        <div class="modal-header">
          <h4 class="modal-title">confirmez votre choix</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>voulez-vous vraiment supprimer l'article&hellip;<span class="art_id"></span></p>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-outline-light" data-dismiss="modal">annuler</button>
          <button type="button" class="btn btn-outline-light" id="modal-validation">supprimer</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->


@endsection
