@extends('admin.A_master')

@section('admin_content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6 pl-2">
        <h1 class="m-0 text-dark">articles supprimés</h1>
      </div><!-- /.col -->
      <div class="col-sm-6 pr-2">
        <a href="{{ url()->previous() }}" class="btn btn-danger header-btn">retoure</a>
        <a href="{{ route('A_article.index') }}" class="btn btn-secondary header-btn">liste article</a>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<table class="table table-striped col-auto">
    <thead>
      <tr class="row">
        <th scope="col">#</th>
        <th scope="col" class="col-5">titre</th>
        <th scope="col" class="col">supprimé à</th>
        <th scope="col" class="col">action</th>
      </tr>
    </thead>
    <tbody>
    @foreach ($articles as $article)
        <tr class="row">
            <th scope="row">{{$article->id}}</th>
            <td class="col-5">{{$article->title}}</td>
            <td class="col">{{$article->deleted_at}}</td>
            <td class="content-fluid col">
                <a href="{{ route('showDeleted', [ $article->id ]) }}" class="btn btn-success" >consulté</a>
            </td>
        </tr>
    @endforeach
    </tbody>
  </table>

@endsection
