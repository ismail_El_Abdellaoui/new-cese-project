@extends('admin.A_master')

@section('admin_content')

  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6 pl-2">
          <h1 class="m-0 text-dark">Modifier l'article</h1>
        </div><!-- /.col -->
        <div class="col-sm-6 pr-2">
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

    <form method="POST" action="{{ route('A_article.update', [ $article->id ]) }}"  enctype="multipart/form-data">
        @csrf
        @method('PUT')        
        
        @include('admin.article.forme')
        <input type="submit" class="btn btn-primary" value="Modifier">
        <a href="{{url()->previous()}}" class="btn btn-danger">annuler</a>

    </form>

@endsection
