<div class="form-group">
    <label for="title">Titre</label>
    <input name="title" type="text" class="form-control" id="title" placeholder="Titre de l'article" value="{{ old('title' , $article->title ?? NULL ) }}">
    @if ($errors->first('title'))
        <span class="error-name">* {{ $errors->first('title') }}</span>
    @endif
  </div>
  <div class="form-group">
    <label for="content">Text</label>
    <section class="">
      <div class="row">
        <div class="col-md-12">
          <div class="card card-outline card-info">
            <div class="card-header">
              <h3 class="card-title">
                Styler votre text
                <small>Simple and fast</small>
              </h3>
              <!-- tools box -->
              <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body pad">
              <div class="mb-3">
                <textarea class="textarea content-fluid" name="content" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" >{{ old('content' , $article->Content ?? NULL ) }}</textarea>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col-->
        <div class="error col-md-12 ">
          @if ($errors->first('content'))
            <span class="error-name">* {{ $errors->first('content') }}</span>
          @endif  
        </div>
      </div>
      <!-- ./row -->
    </section>
  </div>
  <div class="form-group">
      <label for="media">media :</label>
      <input name="media" type="file" class="form-control-file" value="{{ old('content' , $article->media ?? NULL ) }}">
    </div>