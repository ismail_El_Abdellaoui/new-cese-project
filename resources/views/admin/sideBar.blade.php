<aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info user-name">
          
          <li class="dropdown">
            <a id="navbarDropdown" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>

            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
          </li>

        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            @foreach ($menus as $menu)
              @php($sousM = 0)
              @foreach ($menu->sousMenu as $sousmenu)
                @if ( $sousmenu->id )
                    @php($sousM = 1)
                    @break
                @endif
              @endforeach
              @if ($sousM == 1)
                <li class="nav-item has-treeview">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-plus-square"></i>
                    <p>
                      {{ $menu->title }}
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview pl-4">
                    @foreach ($menu->sousMenu as $sousmenu)
                      <li class="nav-item">
                        <a href="{{ route('A_menu.show' , [$sousmenu->id]) }}" class="nav-link">
                          <i class="far fa-circle nav-icon"></i>
                          <p>{{ $sousmenu->title }}</p>
                        </a>
                      </li>
                    @endforeach
                    </ul>
                </li>
              @endif
              @if ($sousM == 0)
                <li class="nav-item">
                  <a href="{{ route('A_menu.show' , [$menu->id]) }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>{{ $menu->title }}</p>
                  </a>
                </li>
              @endif
            @endforeach
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>