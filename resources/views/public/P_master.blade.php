<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" href="{{ mix('css/bootstrap.min.css')}}"> 
    <link rel="stylesheet" href="{{ mix('css/P_style.css')}}"> 
    <link rel="stylesheet" href="{{ mix('css/slick_swiper.css')}}"> 
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    
    <title>CESE</title>
</head>
<body>
  <div class="row header mx-0">
{{-- langue --}}
      <div class="col-sm-2 col-md-2 col-lg-1 col-xl-1 pl-3 ">
          <div class="langue langue-active">
              <a href="">FR</a>
          </div>
          <div class="langue px-1">
              <a href="">ع</a>
          </div>
      </div>

{{-- nav bar --}}
      <div class="col-sm-2 col-md-2 col-lg-7 pl-2 pt-3 ">
          <nav class="navbar navbar-expand-lg navbar-light">
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                  <li class="nav-item">
                    <a class="nav-link mynav js-nav" id="accueil" href=" {{ route('P_accueil') }} ">ACCUEIL</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mynav js-nav" id="conseil" href=" {{ route('P_menu.show' , [2]) }} ">LE CONSEIL</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mynav js-nav" id="press" href=" {{ route('P_menu.show' , [3]) }} ">PRESSE</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mynav js-nav" id="production" href=" {{ route('P_menu.show' , [4]) }} ">PRODUCTION</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mynav js-nav" id="event" href=" {{ route('P_menu.show' , [5]) }} ">ÉVÉNEMENTS</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link mynav js-nav" id="dt" href=" {{ route('P_menu.show' , [6]) }} ">DOSSIER THÉMATIQUE</a>
                  </li>
                </ul>
              </div>
          </nav>
      </div>
      <div class="col-sm-5 col-md-4 col-lg-2 pr-1 pt-4 ">
        <div class="input-group">
          <input type="text" class="form-control">
          <div class="input-group-append">
            <span class="input-group-text"><i class="fa fa-search"></i></span>
          </div>
        </div>
      </div>
{{-- end nav --}}

{{-- logo --}}
      <div class="col-sm-3 col-md-4 col-lg-2 pl-3">
          <img src="/../img/logo/FB_logo.jpg" class="logo" alt="logo">
      </div>
  </div>{{-- end header --}}

  <div class="content">

    @yield('home_content')

  </div>



  {{-- footer --}}
  <footer>

    <!-- footer top -->
        <div class="footer-top">
          <nav class="navbar navbar-expand-sm  justify-content-center  py-2">
            <ul class="navbar-nav">
              <li class="nav-item mr-3">
                <b class="nav-link" href="#">A propos du site</b>
              </li>
              <li class="nav-item mr-3">
                <b class="nav-link" href="#">Mentions légales</b>
              </li>
              <li class="nav-item mr-3">
                <b class="nav-link" href="#">Contactez-nous</b>
              </li>
              <li class="nav-item mr-3">
                <b class="nav-link" href="#">Plan du site</b>
              </li>
              <li class="nav-item">
                <b class="nav-link" href="#">FAQ</b>
              </li>
            </ul>
          </nav>
        </div><!-- end footer top -->

    <!-- footer center -->
        <div class="footer-center H-center">
          <div class="social-media">
            <div class="back-social mr-3">
              <i class="fab fa-linkedin-in"></i>
              <span class="social-title">linkedin</span>
            </div>
            <div class="back-social mr-3">
              <i class="fab fa-youtube"></i>
              <span class="social-title">youtube</span>
            </div>
            <div class="back-social mr-3">
              <i class="fab fa-facebook-f"></i>
              <span class="social-title">facebook</span>
            </div>
            <div class="back-social mr-3">
              <i class="fab fa-pinterest-p"></i>
              <span class="social-title">pinterest</span>
            </div>
            <div class="back-social mr-3">
              <i class="fab fa-twitter"></i>
              <span class="social-title">twitter</span>
            </div>
            <div class="back-social mr-3">
              <i class="fab fa-instagram"></i>
              <span class="social-title">instagram</span>
            </div>
            <div class="back-social">
              <i class="fab fa-google-plus-g"></i>
              <span class="social-title">google+</span>
            </div>
          </div>

          <div class="motivation-text">
            Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sit maiores dolores est aliquam temporibus,
              quas dolore voluptatum velit iusto quis ut .
          </div>

          <div class="footer-info">
            <div class="info">
              <i class="fas fa-envelope"></i>
              my.mail@gmail.com
            </div>
            <div class="info">
              <i class="fas fa-phone-alt"></i>
              +212 600 56 35 63
            </div>
            <div class="info">
              <i class="fas fa-home-lg-alt"></i>
              Lorem ipsum dolor, 25 sit amet consectetur adipisicing elit.
            </div>
          </div>

        </div><!-- end footer center -->

        <div class="footer-buttom H-center ">
          <p>
            © 2020 Lorem ipsum dolor, 25 sit amet consectetur adipisicing elit.
          </p>
        </div>
      </footer>


    {{-- my scripts --}}
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

    <script src="{{ mix('js/jquery_bootstrap_font.js')}}"></script>
    <script src="{{ mix('js/slick_swiper.js')}}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="{{ mix('js/P_script.js')}}"></script>
    

  </body>
</html>
