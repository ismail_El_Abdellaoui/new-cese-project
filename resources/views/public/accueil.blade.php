@extends('public.P_master')

@section('home_content')

 <div class="row">
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @php ($c = true)

            @foreach ($carousel as $slid)
            {{-- {{dd( $slid)}} --}}
            @if( $c )
                <div class="carousel-item active">
                @php ($c = false)

            @else
                <div class="carousel-item ">
            @endif
                    <img class="d-block w-100" src="{{asset('/storage//'. $slid->back_img )}}" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                        <div class="row">
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-12">
                                        <p class="carousel-title">{!!$slid->title!!}</p>
                                    </div>
                                    <div class="col-12 carousel-text line-7">
                                        {!!$slid->Content!!}
                                    </div>
                                    <div class="col-12 mt-3">
                                        <a href="{{ route('P_article.show', [ $slid->id ]) }}" class="btn btn-success"> lire la suite</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-12">
                                        @if ( $slid->media_type == 'img' )
                                            <img src="{{asset('/storage//'. $slid->media )}}" class="carousel-media" alt="media">
                                        @endif
                                        @if ( $slid->media_type == 'video' )
                                            <video src="{{asset('/storage//'. $slid->media )}}" class="carousel-media" alt="media">
                                        @endif
                                    </div>
                                    <div class="col-12 pt-2">
                                        <input type="button" class="btn btn-success" value="decouvrir notre site">
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-12">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    </div>
                                    <div class="col-12">
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
 </div>


 <div class=" medias py-5">
    @include('public.accueil.media')
 </div>


@endsection