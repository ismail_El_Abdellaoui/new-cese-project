
    <h2 class="">media</h2>
    
    <div class="row categorie">
        <div class="col-3 media">
            <a href="{{ route('P_media.show', [ 1 ]) }}">
                <div class="img-media-categories">
                    <img src="{{asset('/storage//img5.jpg' )}}" alt="">
                </div>
                
                <p >
                    <span>ACCUEIL</span>
                </p>
                <button class=" pr-1 toggle-article down" id="1">
                    <i class="fas fa-angle-down "></i>
                </button>
            </a>
        </div>
        <div class="col-3 media">
            <a href="{{ route('P_media.show', [ 2 ]) }}">
                <div class="img-media-categories">
                    <img src="{{asset('/storage//img6.jpg' )}}" alt="">
                </div>
                <p >
                    <span>COMMUNIQUÉS DE PRESSE</span>
                </p>
                <button class=" pr-1 toggle-article" id="2">
                    <i class="fas fa-angle-down "></i>
                </button>
            </a>
        </div>
        <div class="col-3 media">
            <a href="{{ route('P_media.show', [ 3 ]) }}">
                <div class="img-media-categories">
                    <img src="{{asset('/storage//img9.jpg' )}}" alt="">
                </div>
                <p >
                    <span>LE CONSEIL DANS LES MÉDIAS</span>
                   
                </p> 
                <button class=" pr-1 toggle-article" id="3">
                    <i class="fas fa-angle-down "></i>
                </button>
            </a>
        </div>
    </div>

    <!-- Swiper -->
    <div class="mx-0 px-4 show-bloc opened" id="accueil-DD" >
        <div class="swiper swiper-slide responsive">
            @foreach ($accueil as $article)
                <div class="swiper-items">
                    <a href="{{ route('P_article.show', [ $article->id ]) }}">
                        <img src="{{asset( '/storage//'. $article->media )}}" alt="">
                        <p class="line-2">{{ $article->title }}</p>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    
    <div class="mx-0 px-4 show-bloc" id="press-DD" >
        <div class="swiper swiper-slide responsive">
            @foreach ($press as $article)
                <div class="swiper-items">
                    <a href="{{ route('P_article.show', [ $article->id ]) }}">
                        <img src="{{asset( '/storage//'. $article->media )}}" alt="">
                        <p class="line-2">{{ $article->title }}</p>
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    
    <div class="mx-0 px-4 show-bloc" id="conseil-DD" >
        <div class="swiper swiper-slide responsive">
            @foreach ($conseil as $article)
                <div class="swiper-items">
                    <a href="{{ route('P_article.show', [ $article->id ]) }}">
                        <img src="{{asset( '/storage//'. $article->media )}}" alt="">
                        <p class="line-2">{{ $article->title }}</p>
                    </a>
                </div>
            @endforeach
        </div>
    </div>