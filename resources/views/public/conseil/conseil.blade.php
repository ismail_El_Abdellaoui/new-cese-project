@extends('public.P_master')

@section('home_content')

{{-- sous menu --}}
@foreach ($sousMenu as $item)
    @if ($item->title)
        @include('public.sous_menus')
        @break
    @endif 
@endforeach
{{-- /.sous menu --}}

{{-- content --}}
<div class="article-content article">
    <h3 class="article-title">{{$article->title}}</h3>
    <div class="body-content">
      <p class="text-content mb-5">
        {!!$article->Content!!}
      </p>
      <img src="{{asset('/storage//'. $article->media )}}" class="article-media" alt="">
    </div>
    
  </div>
  {{-- /.content --}}

@endsection