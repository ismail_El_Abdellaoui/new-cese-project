@extends('public.P_master')

@section('home_content')

{{-- content --}}
<div class="article-content">
    @foreach ($articles as $article)
        <h2 class="articles-title">{{ $article->media_title }} ARTICLES</h2>
        @break
    @endforeach

    @foreach ($articles as $article)
        <div class="" >
            <a href="{{ route('P_article.show', [ $article->id ]) }}" class="row articles pr-3">
                <div class="col-auto">
                    <img src="{{asset('/storage//'. $article->media )}}" class="articles-media" alt="">
                </div>
                
                <div class="col articles-content">
                    <h3 class="article-title">
                        {!!$article->title!!}
                    </h3>
                    <div class="articles-text-content length media-articles" >{!!$article->Content!!}</div>
                </div>
            </a>
            
            
        </div>
    
    @endforeach
    
  </div>
  {{-- /.content --}}

@endsection