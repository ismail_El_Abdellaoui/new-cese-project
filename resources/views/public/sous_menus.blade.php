<div class="sous-menu">
    <nav class="navbar navbar-expand-lg navbar-light">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav sous-nav">
              @foreach ($sousMenu as $item)
                <li class="nav-item">
                    <a class="nav-link mynav" href="#">{{ $item->title }}</a>
                </li>
              @endforeach
          </ul>
        </div>
    </nav>
</div>