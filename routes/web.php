<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/welcome', function () {
    return view('welcome');
});


// public route
Route::get('/', 'Pub\AccueilController@index')->name('P_accueil');
Route::resource('/P_article' , 'Pub\ArticleController')->only('index','show');
Route::resource('/P_media' , 'Pub\MediaArticleController')->only('show');
Route::resource('/P_menu' , 'Pub\MenuController')->only('show');


// admin route
    // auth
    //      ...->middleware('auth');

Route::resource('/A_menu' , 'Admin\AdminMenuController')->only('index','show');
Route::resource('/A_article' , 'Admin\ArticleController')->middleware('auth');
Route::get('/A_articleDeleted' , 'Admin\ArticleController@deletedArticle')->name('A_articlesDeleted');
Route::get('/A_articleDeleted/{id}' , 'Admin\ArticleController@showDeleted')->name('showDeleted');
Route::resource('/A_accueil' , 'Admin\Accueil\AccueilController')->only('index');
Route::resource('/A_carousel' , 'Admin\Accueil\CarouselController')->only('index','edit' , 'store' ,'newSlide');
Route::get('/A_carousel_newSlide' , 'Admin\Accueil\CarouselController@newSlide')->name('newSlide');
Route::resource('/A_media' , 'Admin\Accueil\MediaController')->only('index','show' , 'create', 'store' );
Route::post('/A_media_delete', 'Admin\Accueil\MediaController@delete')->name('A_media_delete');


// Route::get('/addArticle', function ($menus) {
//     return view('admin.article.addArticle' , [ 'menus' => $menus]);
// })->name('addArticle');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
