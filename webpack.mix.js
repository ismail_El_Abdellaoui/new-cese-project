const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');


// style
mix.styles('resources/sass/bootstrap.min.css','public/css/bootstrap.min.css');
// public
mix.styles(['resources/sass/public/slick.css',
            'resources/sass/public/swiper/slick-theme.css'],'public/css/slick_swiper.css');
mix.styles(['resources/sass/public/P_master_style.css',
            'resources/sass/public/accueil.css',
            'resources/sass/public/P_article.css'],'public/css/P_style.css');
// admin
mix.styles(['resources/sass/admin/A_global.css',
            'resources/sass/admin/A_master.css',
            'resources/sass/admin/A_article.css',
            'resources/sass/admin/accueil/carousel.css'],'public/css/admin/A_style.css');
 
 

// script
mix.js(['resources/js/jquery.js',
        'resources/js/bootstrap.js',
        'resources/js/font.js'], 'public/js/jquery_bootstrap_font.js');
// public
mix.js('resources/js/slick.min.js', 'public/js/slick_swiper.js');
mix.js(['resources/js/public/accueil.js',
        'resources/js/public/article.js',
        'resources/js/public/P_master.js'], 'public/js/P_script.js'); 
// admin
mix.js(['resources/js/admin/article.js',
        'resources/js/admin/carousel.js'], 'public/js/A_script.js');